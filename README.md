# Rust Amazon SP API

> Important! This library is very much `work in progress`, so expect missing features and bugs.

This is a rust library for interacting with the [Amazon Selling Partner API][SP API]

[SP API]: https://developer-docs.amazon.com/sp-api/docs/what-is-the-selling-partner-api

## Examples

### Get Report

```rust
use amazon_sp_api::{
     SellingPartner,
     MarketplaceId,
     reports::{GetReportQueryBuilder, ReportType, get_reports},
}

let sp = SellingPartner::new(Region::EU);
let query = GetReportQueryBuilder::default()
     .report_types(&[ReportType::InventoryLedgerReportSummaryView])
     .marketplace_ids(&[MarketplaceId::ES])
     .created_since("2023-01-01");
     .build()
     .unwrap();
let reports = get_reports(&sp, &query).await?;
```
