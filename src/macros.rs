macro_rules! call_api {
    ($method:ident, $sp:ident, $endpoint:expr) => {{
        let req = $sp
            .client
            .$method(&format!("{}/{}", $sp.base_url(), $endpoint));
        call_api!(req)
    }};
    ($method:ident, $sp:ident, $endpoint:expr, query=$query:expr) => {{
        let req = $sp
            .client
            .$method(&format!("{}/{}", $sp.base_url(), $endpoint))
            .query(&$query.as_query());
        call_api!(req)
    }};
    ($method:ident, $sp:ident, $endpoint:expr, body=$json:expr) => {{
        let req = $sp
            .client
            .$method(&format!("{}/{}", $sp.base_url(), $endpoint))
            .json($json);
        call_api!(req)
    }};
    ($request:expr) => {{
        let bytes = $request.send().await?.bytes().await?;
        Ok(serde_json::from_slice(&bytes)?)
    }};
}

macro_rules! impl_serde_for_enum {
    ($enum:ident) => {
        impl<'de> Deserialize<'de> for $enum {
            fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
            where
                D: serde::Deserializer<'de>,
            {
                use std::str::FromStr;
                let s = String::deserialize(deserializer)?;
                Self::from_str(&s).map_err(|e| serde::de::Error::custom(e.to_string()))
            }
        }

        impl Serialize for $enum {
            fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
            where
                S: serde::Serializer,
            {
                serializer.serialize_str(self.into())
            }
        }
    };
}

pub(crate) use call_api;
pub(crate) use impl_serde_for_enum;
