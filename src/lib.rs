#![doc = include_str!("../README.md")]

#[macro_use]
extern crate derive_builder;

mod macros;
pub mod reports;

use macros::impl_serde_for_enum;
use serde::{Deserialize, Serialize};
use strum::{Display, EnumString, IntoStaticStr};

pub type OperationResult<T> = Result<ApiResponse<T>, Box<dyn std::error::Error>>;

pub trait AsQuery {
    fn as_query(&self) -> Vec<(&'static str, String)>;
}

pub trait AsQueryString {
    fn as_query_str(&self) -> String;
}

impl<T> AsQueryString for [T]
where
    T: ToString,
{
    fn as_query_str(&self) -> String {
        self.iter().map(T::to_string).collect::<Vec<_>>().join(",")
    }
}

impl<T> AsQueryString for T
where
    T: ToString,
{
    #[inline]
    fn as_query_str(&self) -> String {
        self.to_string()
    }
}

impl_serde_for_enum!(MarketplaceId);

pub struct SellingPartner {
    region: Region,
    pub(crate) client: reqwest::Client,
}

impl SellingPartner {
    pub fn new(region: Region) -> Self {
        Self {
            region,
            client: reqwest::Client::new(),
        }
    }

    pub(crate) fn base_url(&self) -> &'static str {
        match self.region {
            Region::EU => "sellingpartnerapi-eu.amazon.com",
            Region::NA => "sellingpartnerapi-na.amazon.com",
            Region::FE => "sellingpartnerapi-fe.amazon.com",
        }
    }
}

#[derive(Debug, Deserialize)]
pub struct ApiResponse<T> {
    pub payload: Option<T>,
    pub errors: Option<Vec<String>>,
}

#[derive(Debug, Deserialize)]
pub struct ApiError {
    pub code: String,
    pub message: String,
    pub details: String,
}

#[derive(Debug, PartialEq, Eq)]
pub enum Region {
    EU,
    NA,
    FE,
}

#[derive(Debug, PartialEq, Eq, EnumString, Display, IntoStaticStr)]
pub enum MarketplaceId {
    #[strum(to_string = "A2EUQ1WTGCTBG2")]
    CA,
    #[strum(to_string = "ATVPDKIKX0DER")]
    US,
    #[strum(to_string = "A1AM78C64UM0Y8")]
    MX,
    #[strum(to_string = "A2Q3Y263D00KWC")]
    BR,
    #[strum(to_string = "A1RKKUPIHCS9HS")]
    ES,
    #[strum(to_string = "A1F83G8C2ARO7P")]
    UK,
    #[strum(to_string = "A13V1IB3VIYZZH")]
    FR,
    #[strum(to_string = "AMEN7PMS3EDWL")]
    BE,
    #[strum(to_string = "A1805IZSGTT6HS")]
    NL,
    #[strum(to_string = "A1PA6795UKMFR9")]
    DE,
    #[strum(to_string = "APJ6JRA9NG5V4")]
    IT,
    #[strum(to_string = "A2NODRKZP88ZB9")]
    SE,
    #[strum(to_string = "A1C3SOZRARQ6R3")]
    PL,
    #[strum(to_string = "ARBP9OOSHTCHU")]
    EG,
    #[strum(to_string = "A33AVAJ2PDY3EV")]
    TR,
    #[strum(to_string = "A17E79C6D8DWNP")]
    SA,
    #[strum(to_string = "A2VIGQ35RCS4UG")]
    AE,
    #[strum(to_string = "A21TJRUUN4KGV")]
    IN,
    #[strum(to_string = "A19VAU5U5O7RUS")]
    SG,
    #[strum(to_string = "A39IBJ37TRP1C6")]
    AU,
    #[strum(to_string = "A1VC38T7YXB528")]
    JP,
}
