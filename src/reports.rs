use crate::macros::{call_api, impl_serde_for_enum};
use crate::{AsQuery, AsQueryString, MarketplaceId, OperationResult, SellingPartner};
use amazon_sp_api_derive::AsQuery;
use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use strum::{Display, EnumString, IntoStaticStr};

impl_serde_for_enum!(ReportType);
impl_serde_for_enum!(ProcessingStatus);
impl_serde_for_enum!(Period);

type ReportOpions<'a> = HashMap<&'a str, &'a str>;

#[derive(Debug, PartialEq, Eq, Clone, EnumString, IntoStaticStr, Display)]
pub enum ReportType {
    #[strum(to_string = "GET_BRAND_ANALYTICS_MARKET_BASKET_REPORT")]
    MarketBasketAnalysisReport,
    #[strum(to_string = "GET_BRAND_ANALYTICS_SEARCH_TERMS_REPORT")]
    AmazonSearchTermsReport,
    #[strum(to_string = "GET_BRAND_ANALYTICS_REPEAT_PURCHASE_REPORT")]
    RepeatPurchase,
    #[strum(to_string = "GET_VENDOR_SALES_REPORT")]
    VendorSalesReport,
    #[strum(to_string = "GET_VENDOR_NET_PURE_PRODUCT_MARGIN_REPORT")]
    NetPureProductMarginReport,
    #[strum(to_string = "GET_VENDOR_TRAFFIC_REPORT")]
    VendorTrafficReport,
    #[strum(to_string = "GET_VENDOR_FORECASTING_REPORT")]
    VendorForecastingReport,
    #[strum(to_string = "GET_VENDOR_INVENTORY_REPORT")]
    VendorInventoryReport,
    #[strum(to_string = "GET_SALES_AND_TRAFFIC_REPORT")]
    SalesandTrafficBusinessReport,
    #[strum(to_string = "GET_FLAT_FILE_OPEN_LISTINGS_DATA")]
    InventoryReport,
    #[strum(to_string = "GET_MERCHANT_LISTINGS_ALL_DATA")]
    AllListingsReport,
    #[strum(to_string = "GET_MERCHANT_LISTINGS_DATA")]
    ActiveListingsReport,
    #[strum(to_string = "GET_MERCHANT_LISTINGS_INACTIVE_DATA")]
    InactiveListingsReport,
    #[strum(to_string = "GET_MERCHANT_LISTINGS_DATA_BACK_COMPAT")]
    OpenListingsReport,
    #[strum(to_string = "GET_MERCHANT_LISTINGS_DATA_LITE")]
    OpenListingsReportLite,
    #[strum(to_string = "GET_MERCHANT_LISTINGS_DATA_LITER")]
    OpenListingsReportLiter,
    #[strum(to_string = "GET_MERCHANT_CANCELLED_LISTINGS_DATA")]
    CanceledListingsReport,
    #[strum(to_string = "GET_MERCHANTS_LISTINGS_FYP_REPORT")]
    SuppressedListingsReport,
    #[strum(to_string = "GET_PAN_EU_OFFER_STATUS")]
    PanEuropeanEligibilityFBAASINs,
    #[strum(to_string = "GET_MFN_PANEU_OFFER_STATUS")]
    PanEuropeanEligibilitySelffulfilledASINs,
    #[strum(to_string = "GET_REFERRAL_FEE_PREVIEW_REPORT")]
    ReferralFeePreviewReport,
    #[strum(to_string = "GET_FLAT_FILE_ACTIONABLE_ORDER_DATA_SHIPPING")]
    UnshippedOrdersReport,
    #[strum(to_string = "GET_ORDER_REPORT_DATA_INVOICING")]
    ScheduledXMLOrderReportInvoicing,
    #[strum(to_string = "GET_ORDER_REPORT_DATA_TAX")]
    ScheduledXMLOrderReportTax,
    #[strum(to_string = "GET_ORDER_REPORT_DATA_SHIPPING")]
    ScheduledXMLOrderReportShipping,
    #[strum(to_string = "value:GET_FLAT_FILE_ORDER_REPORT_DATA_INVOICING")]
    RequestedorScheduledFlatFileOrderReportInvoicing,
    #[strum(to_string = "GET_FLAT_FILE_ORDER_REPORT_DATA_SHIPPING")]
    RequestedorScheduledFlatFileOrderReportShipping,
    #[strum(to_string = "GET_FLAT_FILE_ORDER_REPORT_DATA_TAX")]
    RequestedorScheduledFlatFileOrderReportTax,
    #[strum(to_string = "GET_FLAT_FILE_ALL_ORDERS_DATA_BY_LAST_UPDATE_GENERAL")]
    FlatFileOrdersByLastUpdateReport,
    #[strum(to_string = "GET_FLAT_FILE_ALL_ORDERS_DATA_BY_ORDER_DATE_GENERAL")]
    FlatFileOrdersByOrderDateReport,
    #[strum(to_string = "GET_FLAT_FILE_ARCHIVED_ORDERS_DATA_BY_ORDER_DATE")]
    FlatFileArchivedOrdersReport,
    #[strum(to_string = "GET_XML_ALL_ORDERS_DATA_BY_LAST_UPDATE_GENERAL")]
    XMLOrdersByLastUpdateReport,
    #[strum(to_string = "GET_XML_ALL_ORDERS_DATA_BY_ORDER_DATE_GENERAL")]
    XMLOrdersByOrderDateReport,
    #[strum(to_string = "GET_FLAT_FILE_PENDING_ORDERS_DATA")]
    FlatFilePendingOrdersReport,
    #[strum(to_string = "GET_PENDING_ORDERS_DATA")]
    XMLPendingOrdersReport,
    #[strum(to_string = "GET_CONVERGED_FLAT_FILE_PENDING_ORDERS_DATA")]
    ConvergedFlatFilePendingOrdersReport,
    #[strum(to_string = "GET_XML_RETURNS_DATA_BY_RETURN_DATE")]
    XMLReturnsReportbyReturnDate,
    #[strum(to_string = "GET_FLAT_FILE_RETURNS_DATA_BY_RETURN_DATE")]
    FlatFileReturnsReportbyReturnDate,
    #[strum(to_string = "GET_XML_MFN_PRIME_RETURNS_REPORT")]
    XMLPrimeReturnsReportbyReturnDate,
    #[strum(to_string = "GET_CSV_MFN_PRIME_RETURNS_REPORT")]
    CSVPrimeReturnsReportbyReturnDate,
    #[strum(to_string = "GET_XML_MFN_SKU_RETURN_ATTRIBUTES_REPORT")]
    XMLReturnAttributesReportbyReturnDate,
    #[strum(to_string = "GET_FLAT_FILE_MFN_SKU_RETURN_ATTRIBUTES_REPORT")]
    FlatFileReturnAttributesReportbyReturnDate,
    #[strum(to_string = "GET_SELLER_FEEDBACK_DATA")]
    FlatFileFeedbackReport,
    #[strum(to_string = "GET_V1_SELLER_PERFORMANCE_REPORT")]
    XMLCustomerMetricsReport,
    #[strum(to_string = "GET_V2_SELLER_PERFORMANCE_REPORT")]
    SellerPerformanceReport,
    #[strum(to_string = "GET_PROMOTION_PERFORMANCE_REPORT")]
    PromotionsPerformanceReport,
    #[strum(to_string = "GET_COUPON_PERFORMANCE_REPORT")]
    CouponsPerformanceReport,
    #[strum(to_string = "GET_V2_SETTLEMENT_REPORT_DATA_FLAT_FILE")]
    FlatFileSettlementReport,
    #[strum(to_string = "GET_V2_SETTLEMENT_REPORT_DATA_XML")]
    XMLSettlementReport,
    #[strum(to_string = "GET_V2_SETTLEMENT_REPORT_DATA_FLAT_FILE_V2")]
    FlatFileV2SettlementReport,
    #[strum(to_string = "FBA Sales Reports")]
    FBASalesReports,
    #[strum(to_string = "GET_AMAZON_FULFILLED_SHIPMENTS_DATA_GENERAL")]
    FBAAmazonFulfilledShipmentsReport,
    #[strum(to_string = "GET_AMAZON_FULFILLED_SHIPMENTS_DATA_INVOICING")]
    FBAAmazonFulfilledShipmentsReportInvoicing,
    #[strum(to_string = "GET_AMAZON_FULFILLED_SHIPMENTS_DATA_TAX")]
    FBAAmazonFulfilledShipmentsReportTax,
    #[strum(to_string = "GET_FLAT_FILE_ALL_ORDERS_DATA_BY_LAST_UPDATE_GENERAL")]
    FlatFileAllOrdersReportbyLastUpdate,
    #[strum(to_string = "GET_FLAT_FILE_ALL_ORDERS_DATA_BY_ORDER_DATE_GENERAL")]
    FlatFileAllOrdersReportbyOrderDate,
    #[strum(to_string = "GET_XML_ALL_ORDERS_DATA_BY_LAST_UPDATE_GENERAL")]
    XMLAllOrdersReportbyLastUpdate,
    #[strum(to_string = "GET_XML_ALL_ORDERS_DATA_BY_ORDER_DATE_GENERAL")]
    XMLAllOrdersReportbyOrderDate,
    #[strum(to_string = "GET_FBA_FULFILLMENT_CUSTOMER_SHIPMENT_SALES_DATA")]
    FBACustomerShipmentSalesReport,
    #[strum(to_string = "GET_FBA_FULFILLMENT_CUSTOMER_SHIPMENT_PROMOTION_DATA")]
    FBAPromotionsReport,
    #[strum(to_string = "GET_FBA_FULFILLMENT_CUSTOMER_TAXES_DATA")]
    FBACustomerTaxes,
    #[strum(to_string = "GET_REMOTE_FULFILLMENT_ELIGIBILITY")]
    RemoteFulfillmentEligibility,
    #[strum(to_string = "GET_AFN_INVENTORY_DATA")]
    FBAAmazonFulfilledInventoryReport,
    #[strum(to_string = "GET_AFN_INVENTORY_DATA_BY_COUNTRY")]
    FBAMultiCountryInventoryReport,
    #[strum(to_string = "GET_LEDGER_SUMMARY_VIEW_DATA")]
    InventoryLedgerReportSummaryView,
    #[strum(to_string = "GET_LEDGER_DETAIL_VIEW_DATA")]
    InventoryLedgerReportDetailedView,
    #[strum(to_string = "GET_RESERVED_INVENTORY_DATA")]
    FBAReservedInventoryReport,
    #[strum(to_string = "GET_FBA_MYI_UNSUPPRESSED_INVENTORY_DATA")]
    FBAManageInventory,
    #[strum(to_string = "GET_FBA_MYI_ALL_INVENTORY_DATA")]
    FBAManageInventoryArchived,
    #[strum(to_string = "GET_RESTOCK_INVENTORY_RECOMMENDATIONS_REPORT")]
    RestockInventoryReport,
    #[strum(to_string = "GET_FBA_FULFILLMENT_INBOUND_NONCOMPLIANCE_DATA")]
    FBAInboundPerformanceReport,
    #[strum(to_string = "GET_STRANDED_INVENTORY_UI_DATA")]
    FBAStrandedInventoryReport,
    #[strum(to_string = "GET_STRANDED_INVENTORY_LOADER_DATA")]
    FBABulkFixStrandedInventoryReport,
    #[strum(to_string = "GET_FBA_STORAGE_FEE_CHARGES_DATA")]
    FBAStorageFeesReport,
    #[strum(to_string = "GET_PRODUCT_EXCHANGE_DATA")]
    GetReportExchangeData,
    #[strum(to_string = "GET_FBA_INVENTORY_PLANNING_DATA")]
    FBAManageInventoryHealthReport,
    #[strum(to_string = "GET_FBA_OVERAGE_FEE_CHARGES_DATA")]
    FBAInventoryStorageOverageFeesReport,
    #[strum(to_string = "GET_FBA_ESTIMATED_FBA_FEES_TXT_DATA")]
    FBAFeePreviewReport,
    #[strum(to_string = "GET_FBA_REIMBURSEMENTS_DATA")]
    FBAReimbursementsReport,
    #[strum(to_string = "GET_FBA_FULFILLMENT_LONGTERM_STORAGE_FEE_CHARGES_DATA")]
    FBALongTermStorageFeeChargesReport,
    #[strum(to_string = "GET_FBA_FULFILLMENT_CUSTOMER_RETURNS_DATA")]
    FBAReturnsReport,
    #[strum(to_string = "GET_FBA_FULFILLMENT_CUSTOMER_SHIPMENT_REPLACEMENT_DATA")]
    FBAReplacementsReport,
    #[strum(to_string = "GET_FBA_RECOMMENDED_REMOVAL_DATA")]
    FBARecommendedRemovalReport,
    #[strum(to_string = "GET_FBA_FULFILLMENT_REMOVAL_ORDER_DETAIL_DATA")]
    FBARemovalOrderDetailReport,
    #[strum(to_string = "GET_FBA_FULFILLMENT_REMOVAL_SHIPMENT_DETAIL_DATA")]
    FBARemovalShipmentDetailReport,
    #[strum(to_string = "GET_FBA_UNO_INVENTORY_DATA")]
    SmallandLightInventoryReport,
    #[strum(to_string = "GET_FBA_SNS_FORECAST_DATA")]
    SubscribeandSaveForecastReport,
    #[strum(to_string = "GET_FBA_SNS_PERFORMANCE_DATA")]
    SubscribeandSavePerformanceReport,
    #[strum(to_string = "GST_MTR_STOCK_TRANSFER_REPORT")]
    GSTMerchantStockTransferReport,
    #[strum(to_string = "GST_MTR_B2B")]
    GSTMerchantTaxReportBusinesstoBusiness,
    #[strum(to_string = "GST_MTR_B2C")]
    GSTMerchantTaxReportBusinesstoCustomer,
    #[strum(to_string = "GET_FLAT_FILE_SALES_TAX_DATA")]
    SalesTaxReport,
    #[strum(to_string = "SC_VAT_TAX_REPORT")]
    AmazonVATCalculationReport,
    #[strum(to_string = "GET_VAT_TRANSACTION_DATA")]
    AmazonVATTransactionsReport,
    #[strum(to_string = "GET_GST_MTR_B2B_CUSTOM")]
    OnDemandGSTMerchantTaxReportB2B,
    #[strum(to_string = "GET_GST_MTR_B2C_CUSTOM")]
    OnDemandGSTMerchantTaxReportB2C,
    #[strum(to_string = "GET_GST_STR_ADHOC")]
    OnDemandStockTransferReport,
    #[strum(to_string = "GET_FLAT_FILE_VAT_INVOICE_DATA_REPORT")]
    FlatFileVATInvoiceDataReportVIDR,
    #[strum(to_string = "GET_XML_VAT_INVOICE_DATA_REPORT")]
    XMLVATInvoiceDataReportVIDR,
    #[strum(to_string = "GET_XML_BROWSE_TREE_DATA")]
    BrowseTreeReport,
    #[strum(to_string = "GET_EASYSHIP_DOCUMENTS")]
    EasyShipReport,
    #[strum(to_string = "GET_EASYSHIP_PICKEDUP")]
    EasyShipPickedUpReport,
    #[strum(to_string = "GET_EASYSHIP_WAITING_FOR_PICKUP")]
    EasyShipWaitingforPickUpReport,
    #[strum(to_string = "RFQD_BULK_DOWNLOAD")]
    ManageQuotesReport,
    #[strum(to_string = "FEE_DISCOUNTS_REPORT")]
    ReferralFeeDiscountsReport,
    #[strum(to_string = "GET_B2B_PRODUCT_OPPORTUNITIES_RECOMMENDED_FOR_YOU")]
    B2BProductOpportunitiesRecommendedforYouReport,
    #[strum(to_string = "GET_B2B_PRODUCT_OPPORTUNITIES_NOT_YET_ON_AMAZON")]
    B2BProductOpportunitiesNotyetonAmazon,
    #[strum(to_string = "GET_EPR_MONTHLY_REPORTS")]
    EPRMonthlyReport,
    #[strum(to_string = "GET_EPR_QUARTERLY_REPORTS")]
    EPRQuarterlyReport,
    #[strum(to_string = "GET_EPR_ANNUAL_REPORTS")]
    EPRAnnualReport,
}

#[derive(Debug, PartialEq, Eq, Clone, EnumString, Display, IntoStaticStr)]
pub enum ProcessingStatus {
    #[strum(to_string = "CANCELLED")]
    Cancelled,
    #[strum(to_string = "DONE")]
    Done,
    #[strum(to_string = "FATAL")]
    Fatal,
    #[strum(to_string = "IN_PROGRESS")]
    InProgress,
    #[strum(to_string = "IN_QUEUE")]
    InQueue,
}

#[derive(Debug, PartialEq, Eq, Clone, EnumString, Display, IntoStaticStr)]
pub enum CompressionAlgorithm {
    #[strum(to_string = "GZIP")]
    GZIP,
}

#[derive(Debug, PartialEq, Eq, Clone, EnumString, Display, IntoStaticStr)]
pub enum Period {
    #[strum(to_string = "PT5M")]
    PT5M,
    #[strum(to_string = "PT15M")]
    PT15M,
    #[strum(to_string = "PT30M")]
    PT30M,
    #[strum(to_string = "PT1H")]
    PT1H,
    #[strum(to_string = "PT2H")]
    PT2H,
    #[strum(to_string = "PT4H")]
    PT4H,
    #[strum(to_string = "PT8H")]
    PT8H,
    #[strum(to_string = "PT12H")]
    PT12H,
    #[strum(to_string = "P1D")]
    P1D,
    #[strum(to_string = "P2D")]
    P2D,
    #[strum(to_string = "P3D")]
    P3D,
    #[strum(to_string = "PT84H")]
    PT84H,
    #[strum(to_string = "P7D")]
    P7D,
    #[strum(to_string = "P14D")]
    P14D,
    #[strum(to_string = "P15D")]
    P15D,
    #[strum(to_string = "P18D")]
    P18D,
    #[strum(to_string = "P30D")]
    P30D,
    #[strum(to_string = "P1M")]
    P1M,
}

#[derive(Debug, Clone, Builder, AsQuery)]
pub struct GetReportQuery<'a> {
    #[builder(setter(strip_option), default)]
    pub report_types: Option<&'a [ReportType]>,
    #[builder(setter(strip_option), default)]
    pub processing_statuses: Option<&'a [ProcessingStatus]>,
    #[builder(setter(strip_option), default)]
    pub marketplace_ids: Option<&'a [MarketplaceId]>,
    #[builder(setter(strip_option), default)]
    pub page_size: Option<u32>,
    #[builder(setter(strip_option), default)]
    pub created_since: Option<&'a str>,
    #[builder(setter(strip_option), default)]
    pub created_until: Option<&'a str>,
    #[builder(setter(strip_option), default)]
    pub next_token: Option<&'a str>,
}

#[derive(Debug, AsQuery)]
pub struct GetReportSchedulesQuery {
    pub report_types: Vec<ReportType>,
}

#[derive(Debug, Clone, Builder, Serialize)]
pub struct CreateReportSpecification<'a> {
    #[serde(rename = "reportOptions")]
    #[builder(setter(strip_option), default)]
    pub report_options: Option<ReportOpions<'a>>,
    #[serde(rename = "reportType")]
    pub report_type: ReportType,
    #[serde(rename = "dataStartTime")]
    #[builder(setter(strip_option), default)]
    pub data_start_time: Option<&'a str>,
    #[serde(rename = "dataEndTime")]
    #[builder(setter(strip_option), default)]
    pub data_end_time: Option<&'a str>,
    #[serde(rename = "marketplaceIds")]
    pub marketplace_ids: &'a [MarketplaceId],
}

#[derive(Debug, Clone, Builder, Serialize)]
pub struct CreateReportScheduleSpecification<'a> {
    #[serde(rename = "reportType")]
    pub report_type: ReportType,
    #[serde(rename = "marketplaceIds")]
    pub marketplace_ids: &'a [MarketplaceId],
    #[serde(rename = "reportOptions")]
    #[builder(setter(strip_option), default)]
    pub report_options: Option<ReportOpions<'a>>,
    pub period: Period,
    #[serde(rename = "nextReportCreationTime")]
    #[builder(setter(strip_option), default)]
    pub next_report_creation_time: Option<&'a str>,
}

#[derive(Debug, Deserialize)]
pub struct CreateReportResponse {
    #[serde(rename = "reportId")]
    pub response_id: String,
}

#[derive(Debug, Deserialize)]
pub struct ReportSchedule {
    #[serde(rename = "reportScheduleId")]
    pub report_schedule_id: String,
    #[serde(rename = "reportType")]
    pub report_type: String,
    #[serde(rename = "marketplaceIds")]
    pub marketplace_ids: Option<Vec<MarketplaceId>>,
    #[serde(rename = "reportOptions")]
    pub report_options: Option<HashMap<String, String>>,
    pub period: String,
    #[serde(rename = "nextReportCreationTime")]
    pub next_report_creation_time: Option<String>,
}

#[derive(Debug, Deserialize)]
pub struct ReportScheduleList {
    #[serde(rename = "reportSchedules")]
    pub report_schedules: Vec<ReportSchedule>,
}

#[derive(Debug, Deserialize)]
pub struct GetReportsResponse {
    pub reports: Vec<Report>,
    #[serde(rename = "nextToken")]
    pub next_token: Option<String>,
}

#[derive(Debug, Deserialize)]
pub struct Report {
    #[serde(rename = "marketplaceIds")]
    pub marketplace_ids: Option<Vec<String>>,
    #[serde(rename = "reportId")]
    pub report_id: String,
    #[serde(rename = "reportType")]
    pub report_type: ReportType,
    #[serde(rename = "dataStartTime")]
    pub data_start_time: Option<String>,
    #[serde(rename = "dataEndTime")]
    pub data_end_time: Option<String>,
    #[serde(rename = "ReportScheduleId")]
    pub report_schedule_id: Option<String>,
    #[serde(rename = "createdTime")]
    pub created_time: String,
    #[serde(rename = "processingStatus")]
    pub processing_status: ProcessingStatus,
    #[serde(rename = "processingStartTime")]
    pub processing_start_time: Option<String>,
    #[serde(rename = "processingEndTime")]
    pub processing_end_time: Option<String>,
    #[serde(rename = "reportDocumentId")]
    pub report_document_id: Option<String>,
}

#[derive(Debug, Deserialize)]
pub struct CreateReportScheduleResponse {
    #[serde(rename = "reportScheduleId")]
    pub report_schedule_id: String,
}

#[derive(Debug, Deserialize)]
pub struct ReportDocument {
    #[serde(rename = "reportDocumentId")]
    pub report_document_id: String,
    pub url: String,
    #[serde(rename = "compressionAlgorithm")]
    pub compression_algorithm: Option<String>,
}

pub async fn get_reports<'a>(
    sp: &SellingPartner,
    query: &GetReportQuery<'a>,
) -> OperationResult<GetReportsResponse> {
    call_api!(get, sp, "reports/2021-06-30/reports", query = query)
}

pub async fn create_report<'a>(
    sp: &SellingPartner,
    body: &CreateReportSpecification<'a>,
) -> OperationResult<CreateReportResponse> {
    call_api!(post, sp, "reports/2021-06-30/reports", body = body)
}

pub async fn get_report<'a>(sp: &SellingPartner, report_id: &str) -> OperationResult<Report> {
    call_api!(get, sp, format!("reports/2021-06-30/reports/{report_id}"))
}

pub async fn cancel_report(sp: &SellingPartner, report_id: &str) -> OperationResult<()> {
    call_api!(
        delete,
        sp,
        format!("reports/2021-06-30/reports/{report_id}")
    )
}

pub async fn get_report_schedules(
    sp: &SellingPartner,
    query: GetReportSchedulesQuery,
) -> OperationResult<ReportScheduleList> {
    call_api!(get, sp, "reports/2021-06-30/schedules", query = query)
}

pub async fn create_report_schedule<'a>(
    sp: &SellingPartner,
    body: &CreateReportScheduleSpecification<'a>,
) -> OperationResult<CreateReportScheduleResponse> {
    call_api!(post, sp, "reports/2021-06-30/schedules", body = body)
}

pub async fn get_report_schedule(
    sp: &SellingPartner,
    report_schedule_id: &str,
) -> OperationResult<ReportSchedule> {
    call_api!(
        get,
        sp,
        format!("reports/2021-06-30/schedules/{report_schedule_id}")
    )
}

pub async fn cancel_report_schedule(
    sp: &SellingPartner,
    report_schedule_id: &str,
) -> OperationResult<()> {
    call_api!(
        delete,
        sp,
        format!("reports/2021-06-30/schedules/{report_schedule_id}")
    )
}

pub async fn get_report_document(
    sp: &SellingPartner,
    report_document_id: &str,
) -> OperationResult<ReportDocument> {
    call_api!(
        get,
        sp,
        format!("reports/2021-06-30/documents/{report_document_id}")
    )
}
