extern crate proc_macro;

use heck::AsLowerCamelCase;
use proc_macro::TokenStream;
use quote::quote;
use syn::{parse_macro_input, Data, DeriveInput, Type};

#[proc_macro_derive(AsQuery, attributes(query))]
pub fn derive_as_query(input: TokenStream) -> TokenStream {
    let input = parse_macro_input!(input as DeriveInput);
    let ident = input.ident;
    let (impl_generics, ty_generics, where_generics) = input.generics.split_for_impl();

    let data = match input.data {
        Data::Struct(s) => s,
        _ => panic!("Only structs are supported"),
    };

    let fields = data.fields.iter().map(|f| {
        let ident = &f.ident.as_ref().unwrap();
        let query_name = AsLowerCamelCase(ident.to_string()).to_string();
        match &f.ty {
            Type::Path(p) => {
                if p.path.segments.first().unwrap().ident == "Option" {
                    quote! {
                        if let Some(#ident) = &self.#ident {
                            query.push((#query_name, #ident.as_query_str()));
                        }
                    }
                } else {
                    quote! {
                        query.push((#query_name, self.#ident.as_query_str()));
                    }
                }
            }
            _ => panic!("Invalid field type"),
        }
    });

    TokenStream::from(quote! {
        #[automatically_derived]
        impl #impl_generics AsQuery for #ident #ty_generics #where_generics {
            fn as_query(&self) -> Vec<(&'static str, String)> {
                let mut query = Vec::new();
                #(#fields)*
                query
            }
        }
    })
}
